package com.udacity

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK


class NotificationButtonReceiver : BroadcastReceiver() {
    companion object {
        const val NOTIFICATION_ID_KEY = "notification_id"
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        val notificationId = intent?.getIntExtra(NOTIFICATION_ID_KEY, 0)
        val manager = context?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationId?.let { manager.cancel(it) }
        val detailIntent = Intent(context, DetailActivity::class.java)
        intent?.extras?.let { detailIntent.putExtras(it) }
        detailIntent.addFlags(FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(detailIntent)
        context.sendBroadcast(Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS))
    }

}