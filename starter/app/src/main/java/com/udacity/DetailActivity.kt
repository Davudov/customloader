package com.udacity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.content_detail.*

class DetailActivity : AppCompatActivity() {
    companion object {
        const val FILE_NAME_KEY = "file_name"
        const val FILE_STATUS_KEY = "file_status"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        setSupportActionBar(toolbar)
        file_name.text = intent.getStringExtra(FILE_NAME_KEY)
        download_status.text = intent.getStringExtra(FILE_STATUS_KEY)
        btn_ok.setOnClickListener { finish() }
        motion_layout.transitionToEnd()
    }

}
