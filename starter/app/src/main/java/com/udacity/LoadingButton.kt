package com.udacity

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import android.view.animation.LinearInterpolator
import kotlinx.coroutines.*
import timber.log.Timber
import kotlin.properties.Delegates


class LoadingButton @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {
    lateinit var downloadListener: DownloadListener
    private val ARC_END_ANGLE = 360f
    private var widthSize = 0
    private var heightSize = 0
    private val rectangle = Rect()
    private val paint = Paint()
    private var currentSweepAngle = 0f
    private var currentProgressPosition = 0
    private val arc = Arc(0f, ARC_END_ANGLE, context.getColor(R.color.colorAccent))
    private val arcRect: RectF = RectF(0f, 0f, 0f, 0f)
    private val START_ANGLE = 0f
    private var status = ""
    private val linearInterpolator = LinearInterpolator()
    private var part = 0f
    private var queque = CompletableDeferred<Boolean>()
    private val COMPLETE_TIMEOUT = 10_000L
    private val textPaint = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.FILL
        textAlign = Paint.Align.CENTER
    }

    private val arcPaint = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.FILL
        textAlign = Paint.Align.CENTER
        color = context.getColor(R.color.colorPrimary)
    }

    private var valueAnimator = ValueAnimator().apply {
        interpolator = linearInterpolator
        addUpdateListener { valueAnimator ->
            currentProgressPosition = (valueAnimator.animatedValue as Float).toInt()
        }
    }

    private val valueAnimatorCircle = ValueAnimator().apply {
        interpolator = linearInterpolator
        addUpdateListener { valueAnimator ->
            currentSweepAngle = valueAnimator.animatedValue as Float
            postInvalidate()
        }
    }


    var buttonState: ButtonState by Delegates.observable<ButtonState>(ButtonState.Completed) { p, old, new ->
        when (new) {
            is ButtonState.Clicked -> {
                onDownloadClicked()
                isEnabled = false
            }
            is ButtonState.Loading -> {
                onDownloadResume()
            }
            is ButtonState.Completed -> {
                onDownloadComplete()
            }
        }
        postInvalidate()
    }


    init {
        widthSize = width
        heightSize = height
        if (attrs != null) {
            val ta = context.obtainStyledAttributes(attrs, R.styleable.LoadingButton)
            val backgroundColor = ta.getColor(
                    R.styleable.LoadingButton_background_color,
                    context.getColor(R.color.colorPrimary)
            )
            val progressColor = ta.getColor(
                    R.styleable.LoadingButton_progress_color,
                    context.getColor(R.color.colorPrimaryDark)
            )
            val textColor = ta.getColor(
                    R.styleable.LoadingButton_text_color, Color.WHITE
            )
            val textSizeAttr = ta.getDimension(
                    R.styleable.LoadingButton_text_size, 100f
            )
            setBackgroundColor(backgroundColor)
            paint.color = progressColor
            textPaint.apply {
                color = textColor
                textSize = textSizeAttr
            }
            status = "Download"
            ta.recycle()
        }

    }


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        rectangle.left = 0
        rectangle.top = 0
        rectangle.right = currentProgressPosition
        rectangle.bottom = heightSize
        canvas?.drawRect(rectangle, paint)

        val xPos = (width / 2).toFloat()
        val yPos = ((height / 2 - (textPaint.descent() + textPaint.ascent()) / 2))
        canvas?.drawText(status, xPos, yPos, textPaint)
        //
        val textWidth = textPaint.measureText(status)
        val radius = textPaint.textSize / 2
        //
        val xPosCircle = xPos + textWidth / 2f + radius
        val yPosCircle = (height / 2).toFloat()

        arcRect.set(
                xPosCircle - radius,
                yPosCircle - radius,
                xPosCircle + radius,
                yPosCircle + radius
        )

        if (currentSweepAngle > arc.start + arc.sweep) {
            canvas?.drawArc(
                    arcRect,
                    START_ANGLE + arc.start,
                    arc.sweep,
                    true,
                    arcPaint
            )
            arcPaint.color = arc.color
        } else {
            if (currentSweepAngle > arc.start) {
                canvas?.drawArc(
                        arcRect,
                        START_ANGLE + arc.start,
                        currentSweepAngle - arc.start,
                        true,
                        arcPaint
                )
                arcPaint.color = arc.color
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val minw: Int = paddingLeft + paddingRight + suggestedMinimumWidth
        val w: Int = resolveSizeAndState(minw, widthMeasureSpec, 1)
        val h: Int = resolveSizeAndState(
                MeasureSpec.getSize(w),
                heightMeasureSpec,
                0
        )
        widthSize = w
        heightSize = h
        setMeasuredDimension(w, h)
    }


    private fun onDownloadClicked() {

        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                val duration1 = startStage(.4f, 500)
                delay(duration1)
                delay(duration1)
                val duration2 = startStage(.3f, 600)
                delay(duration2)
                delay(duration2)
                val duration3 = startStage(.2f, 200)
                delay(duration3)
                try {
                    var taskCompleted = false
                    withTimeout(COMPLETE_TIMEOUT) {
                        taskCompleted = queque.await()
                    }
                    if (taskCompleted) {
                        completeAnimationWithStatus()
                    } else {
                        completeAnimationWithStatus("Something went wrong. Try Again", true)
                    }
                } catch (e: Exception) {
                    Timber.e(e)
                    completeAnimationWithStatus("Something went wrong. Try Again", true)
                }
            }
        }

    }

    private suspend fun completeAnimationWithStatus(completeStatus: String = "Download", failed: Boolean = false) {
        val duration4 = startStage(.1f, 500)
        delay(duration4)
        delay(startStage(0f, 50))
        status = completeStatus
        queque = CompletableDeferred()
        if (failed) downloadListener.onFail()
        isEnabled = true
    }

    private fun onDownloadResume() {
        status = "We are loading"
    }

    private fun onDownloadComplete() {
        queque.complete(true)
    }


    private fun startStage(parcel: Float, duration: Long = 1000L): Long {
        val lastPart = part
        valueAnimator.duration = duration
        valueAnimatorCircle.duration = duration
        valueAnimator.setFloatValues(lastPart * widthSize, widthSize * part.let {
            part += parcel
            part
        })
        valueAnimatorCircle.setFloatValues(lastPart * ARC_END_ANGLE, ARC_END_ANGLE * part)
        startAnimate()
        if (part >= 1f) {
            part = 0f
        }
        return duration
    }

    private fun startAnimate() {
        valueAnimatorCircle.start()
        valueAnimator.start()
    }
}

private class Arc(val start: Float, val sweep: Float, val color: Int)
