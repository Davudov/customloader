package com.udacity

import android.app.DownloadManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import kotlinx.android.synthetic.main.content_main.*
import timber.log.Timber

class DownloaderReceiver(private val downloadListener: DownloadListener) :
        BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val action = intent.action
        if (action == DownloadManager.ACTION_DOWNLOAD_COMPLETE) {
            try {
                listenStatus(context, intent)
            } catch (e: Exception) {
                Timber.e(e)
                downloadListener.onFail()
            }
        }
    }

    private fun listenStatus(context: Context, intent: Intent) {
        val query = DownloadManager.Query()
        query.setFilterById(intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0))
        val manager =
                context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        val cursor = manager.query(query)
        if (cursor.moveToFirst()) {
            if (cursor.count > 0) {
                val status =
                        cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))
                val total =
                        cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES))
                Timber.d("status - total $total")
                val downloaded =
                        cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR))
                Timber.d("status - downloaded $downloaded")

                when (status) {
                    DownloadManager.STATUS_RUNNING -> {
                        Timber.d("status - STATUS_RUNNING")

                    }
                    DownloadManager.STATUS_PENDING -> {
                        Timber.d("status - STATUS_PENDING")

                    }
                    DownloadManager.STATUS_SUCCESSFUL -> {
                        Timber.d("status - STATUS_SUCCESSFUL")
                        downloadListener.onSuccess()
                    }
                    DownloadManager.STATUS_FAILED -> {
                        Timber.d("status - STATUS_FAILED")

                    }
                }
            }
        }
        cursor.close()
    }


}