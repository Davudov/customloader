package com.udacity

import android.app.DownloadManager
import android.app.PendingIntent
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.widget.RadioButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.udacity.DetailActivity.Companion.FILE_NAME_KEY
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import timber.log.Timber


class MainActivity : AppCompatActivity(), DownloadListener {

    private var downloadID: Long = 0

    private lateinit var notificationReceiver: Intent

    private var onNotSelected = true

    private lateinit var downloaderReceiver: DownloaderReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        downloaderReceiver = DownloaderReceiver(this)
        registerReceiver(
                downloaderReceiver,
                IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
        )
        custom_button.downloadListener = this

        notificationReceiver = Intent(this, NotificationButtonReceiver::class.java)



        custom_button.setOnClickListener {
            custom_button.buttonState = ButtonState.Clicked
            custom_button.buttonState = ButtonState.Loading
            try {
                val selectedBtn = radio_group.findViewById<RadioButton>(radio_group.checkedRadioButtonId)
                onNotSelected = selectedBtn == null
                val url = selectedBtn.tag
                download(url.toString())
                notificationReceiver.putExtra(FILE_NAME_KEY, selectedBtn.text.toString())
            } catch (e: Exception) {
                Toast.makeText(this, "Please select the file to download", Toast.LENGTH_SHORT).show()
                Timber.e(e)
            }
        }


    }

    private fun download(url: String = URL) {
        val request =
                DownloadManager.Request(Uri.parse(url))
                        .setTitle(getString(R.string.app_name))
                        .setDescription(getString(R.string.app_description))
                        .setRequiresCharging(false)
                        .setAllowedOverMetered(true)
                        .setAllowedOverRoaming(true)
        val downloadManager = getSystemService(DOWNLOAD_SERVICE) as DownloadManager

        downloadID =
                downloadManager.enqueue(request)// enqueue puts the download request in the queue.


    }


    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(downloaderReceiver)
    }


    companion object {
        private const val URL =
                "https://github.com/udacity/nd940-c3-advanced-android-programming-project-starter/archive/master.zip"
        const val CHANNEL_ID = "channelId"
    }

    override fun onSuccess() {
        custom_button.buttonState = ButtonState.Completed
        showNotification()
    }

    override fun onFail() {
        if (!onNotSelected)
            showNotification(true)
    }

    private fun showNotification(failed: Boolean = false) {

        notificationReceiver.putExtra(NotificationButtonReceiver.NOTIFICATION_ID_KEY, downloadID.toInt())

        if (failed)
            notificationReceiver.putExtra(DetailActivity.FILE_STATUS_KEY, "Failed")
        else notificationReceiver.putExtra(DetailActivity.FILE_STATUS_KEY, "Success")


        val pendingIntent = PendingIntent.getBroadcast(this, 1,
                notificationReceiver, PendingIntent.FLAG_UPDATE_CURRENT)

        val builder = NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_assistant_black_24dp)
                .setContentTitle("Udacity: Android Kotlin Nanodegree")
                .setContentText("The Project 3 repository downloaded")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .addAction(R.drawable.ic_assistant_black_24dp, "Check the status", pendingIntent)
                .setAutoCancel(false)

        NotificationManagerCompat.from(this).apply {
            notify(downloadID.toInt(), builder.build())
        }
    }

}

interface DownloadListener {
    fun onSuccess()
    fun onFail()
}